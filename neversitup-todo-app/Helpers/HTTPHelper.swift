//
//  HTTPHelper.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 12/10/2564 BE.
//

import Foundation
import Alamofire

class HttpHelper {
  static func commonHeader() -> HTTPHeaders {
    var headers: HTTPHeaders = HTTPHeaders()
    if let token = UserHelper.getUserToken() {
      headers.update(name: "Authorization", value: "Bearer \(token)")
    }
    headers.update(name: "Content-Type", value: "application/json")
    return headers
  }
}

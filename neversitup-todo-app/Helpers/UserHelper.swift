//
//  UserHelper.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 10/10/2564 BE.
//

import Foundation

public final class UserHelper {
  enum UserInfoKey: String {
    case userToken
    case username
    case password
  }
  
  static func getUserToken() -> String? {
    return UserDefaults.standard.string(forKey: UserInfoKey.userToken.rawValue)
  }
  
  static func updateUserToken(token: String) {
    UserDefaults.standard.setValue(token, forKey: UserInfoKey.userToken.rawValue)
  }
  
  static func getCurrentUser() -> Login? {
    let storedEmail = UserDefaults.standard.string(forKey: UserInfoKey.username.rawValue)
    let storedPassword = UserDefaults.standard.string(forKey: UserInfoKey.password.rawValue)
    guard let email = storedEmail, let password = storedPassword else { return nil }
    return Login(email: email, password: password)
  }
  
  static func updateCurrentUser(email: String, password: String) {
    UserDefaults.standard.setValue(email, forKey: UserInfoKey.username.rawValue)
    UserDefaults.standard.setValue(password, forKey: UserInfoKey.password.rawValue)
  }
  
}

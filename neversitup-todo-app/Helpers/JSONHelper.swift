//
//  JSONHelper.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

func dataFrom(path: String) -> Data? {
  do {
    let url = URL(fileURLWithPath: path)
    return try Data(contentsOf: url)
  } catch (let error) {
    print(error)
    return nil
  }
}

func jsonObjectFrom(path: String) -> [String: AnyObject]? {
  do {
    guard let url = URL(string: path) else { return nil }
    let data = try Data(contentsOf: url)
    let json = try JSONSerialization.jsonObject(with: data, options: [])
    return json as? [String : AnyObject]
  } catch {
    return nil
  }
}

//
//  Result.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

enum Result<T> {
  case success(result: T)
  case failure(error: Error?)
}

enum UserResult<T> {
  case success(result: T)
  case failure(error: Error?)
  case empty
}

enum Content<T> {
  case loading
  case empty
  case error
  case errorMessage(message: String?)
  case success(data: T)
}

//
//  StringExtension.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

extension String {
  func withBaseURL() -> String {
    return "\(BASE_URL)\(self.hasPrefix("/") ? "" : "/")\(self)"
  }
}

//
//  UserWorker.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

protocol UserProtocol {
  func login(request: Login, completion: @escaping (Result<User>) -> Void)
  func register(request: Register, completion: @escaping (Result<User>) -> Void)
  func validateToken(token: String, completion: @escaping (Result<UserInfo>) -> Void)
}

class UserWorker {
  var store: UserProtocol?
  
  init(with store: UserProtocol) {
    self.store = store
  }
  
  func login(request: Login, completion: @escaping (Result<User>) -> Void) {
    store?.login(request: request, completion: { (result) in
      switch result {
      case .success(let userInfo):
        completion(.success(result: userInfo))
      case .failure(let error):
        completion(.failure(error: error))
      }
    })
  }
  
  func register(request: Register, completion: @escaping (Result<User>) -> Void) {
    store?.register(request: request, completion: { (result) in
      switch result {
      case .success(let userInfo):
        completion(.success(result: userInfo))
      case .failure(let error):
        completion(.failure(error: error))
      }
    })
  }
  
  func validateToken(token: String, completion: @escaping (Result<UserInfo>) -> Void) {
    store?.validateToken(token: token, completion: { (result) in
      switch result {
      case .success(let userInfo):
        completion(.success(result: userInfo))
      case .failure(let error):
        completion(.failure(error: error))
      }
    })
  }
}

//
//  MainWorker.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol TodoListProtocol {
  func getTodoList(completion: @escaping (Result<TodoList>) -> Void)
}

class TodoListWorker {
  var store: TodoListProtocol?
  
  init(with store: TodoListProtocol) {
    self.store = store
  }
  
  func getTodoList(completion: @escaping (Result<TodoList>) -> Void) {
    store?.getTodoList(completion: { (result) in
      switch result {
      case .success(let data):
        completion(.success(result: data))
      case .failure(let error):
        completion(.failure(error: error))
      }
    })
  }
}

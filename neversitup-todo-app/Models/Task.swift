//
//  Task.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

struct TodoList: Codable {
  var count: Int
  var data: [Task]
}

struct UpdateTask: Codable {
  var success: Bool
}

struct GetTask: Codable {
  var success: Bool
  var data: Task
}

struct Task: Codable {
  var completed: Bool
  var _id: String
  var description: String
  var owner: String
  var createdAt: String
  var updatedAt: String
  
  init(completed: Bool, _id: String, description: String, owner: String, createdAt: String, updatedAt: String) {
    self.completed = completed
    self._id = _id
    self.description = description
    self.owner = owner
    self.createdAt = createdAt
    self.updatedAt = updatedAt
  }
}

struct UpdateRequest: Encodable {
  let completed: Bool
  let description: String?
}

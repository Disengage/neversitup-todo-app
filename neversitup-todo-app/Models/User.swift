//
//  User.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

struct Login: Codable {
  let email: String
  let password: String
  
  init(email: String, password: String) {
    self.email = email
    self.password = password
  }
}

struct Register: Codable {
  let name: String
  let email: String
  let password: String
  let age: Int
}

struct User: Codable {
  let user: UserInfo
  let token: String
}

struct UserInfo: Codable {
  let age: Int
  let _id: String
  let name: String
  let email: String
  let createdAt: String
  let updatedAt: String
}

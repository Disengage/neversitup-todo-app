//
//  Common.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 12/10/2564 BE.
//

import Foundation

enum EditOptions {
  case add(newTask: Task)
  case update(updatedTask: Task)
}

enum DisplayMode: Equatable {
  static func == (lhs: DisplayMode, rhs: DisplayMode) -> Bool {
    if case .listMode = lhs, case .listMode = rhs {
      return true
    }
    return false
  }
  
  case listMode
  case calendarMode
  case doneListMode
  case editMode(options: EditOptions?)
}

//
//  StartUpController.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MBProgressHUD

class StartUpController: UIViewController {
  // MARK: View lifecycle
  let worker = UserWorker(with: UserStore())
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.setNavigationBarHidden(true, animated: false)
    
    if let token = UserHelper.getUserToken(), !token.isEmpty {
      worker.validateToken(token: token) { [weak self] (result) in
        switch result {
        case .success(let userInfo):
          print("Login with: \(userInfo)")
          self?.navigateToMain(info: userInfo)
        default:
          self?.navigateToLogin()
        }
      }
    } else {
      navigateToLogin()
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    MBProgressHUD.showAdded(to: self.view, animated: false)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    MBProgressHUD.hide(for: self.view, animated: false)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    var viewControllers = self.navigationController?.viewControllers
    viewControllers?.removeFirst()
    self.navigationController?.viewControllers = viewControllers ?? []
  }
  
  func navigateToLogin() {
    performSegue(withIdentifier: "login", sender: nil)
  }
  
  func navigateToMain(info: UserInfo) {
    performSegue(withIdentifier: "main", sender: info)
  }
}

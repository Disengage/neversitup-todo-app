//
//  MainViewController.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MBProgressHUD

protocol MainDisplayLogic: class {
  func displayLoading(isLoading: Bool)
  func displayTodoList(viewModel: Main.GetTodoList.ViewModel)
  func displayDeleteTask(viewModel: Main.DeleteTodoTask.ViewModel)
  func displayTaskDetail(viewModel: Main.GetTaskDetail.ViewModel)
  func displayAllTaskDone(viewModel: Main.SetAllTaskDone.ViewModel)
  func displayLogout(viewModel: Main.Logout.ViewModel)
}

class MainViewController: UIViewController, MainDisplayLogic {
  var interactor: MainBusinessLogic?
  var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?

  // MARK: UI
  @IBOutlet weak var mainTableView: UITableView!
  
  @IBOutlet weak var menuContainerHeight: NSLayoutConstraint!
  @IBOutlet weak var menuContainerView: UIView!
  
  private weak var menuViewController: MenuViewController?
  
  private var displayMode: DisplayMode = .listMode {
    didSet {
      if case .listMode = displayMode {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "All", style: .plain, target: self, action: #selector(navigateToDoneTask))
      } else if case .calendarMode  = displayMode {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(navigateToLogin))
      } else {
        navigationItem.rightBarButtonItem = nil
      }
    }
  }
  
  private var todoList: [DisplayedListCell] = []
  
  private var isUpdatingTask: Bool = false {
    didSet {
      if isUpdatingTask {
        navigationItem.rightBarButtonItem = nil
      }
    }
  }
  
  private var selectedList: [Int: Bool] = [:] {
    didSet {
      let selected = selectedList.filter({ $0.value })
      if selected.count > 0 {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(markAllTaskDone))
      } else {
        navigationItem.rightBarButtonItem = nil
      }
    }
  }
  
  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    displayMode = router?.dataStore?.displayMode ?? .listMode
    setupNavController()
    setupTableView()
    setupUI()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    let tasks: [Task]? = router?.dataStore?.originalTasks
    interactor?.getTodoList(request: Main.GetTodoList.Request(style: displayMode, task: tasks))
  }
  
  private func setupNavController() {
    var viewControllers = self.navigationController?.viewControllers
    viewControllers = viewControllers?.filter({ $0.self is MainViewController })
    self.navigationController?.viewControllers = viewControllers ?? []
    navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
  private func setupUI() {
    menuContainerHeight.constant = 140
    menuContainerView.updateConstraints()
    
    menuViewController?.delegate = self
    
    switch displayMode {
    case .listMode:
      title = "TODO"
      menuContainerView.isHidden = false
    case .doneListMode:
      title = "DONE TASKS"
      menuContainerView.isHidden = true
    default:
      break
    }
  }
  
  private func setupTableView() {
    mainTableView.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
    mainTableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 140, right: 0)
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if let emptyView = storyboard.instantiateViewController(withIdentifier: "EmptyListView") as? EmptyListViewController {
      mainTableView.backgroundView = emptyView.view
    }
    mainTableView.backgroundView?.isHidden = true
  }
  
  // MARK: Displayed
  func displayLoading(isLoading: Bool) {
    if isLoading {
      MBProgressHUD.showAdded(to: self.view, animated: true)
    } else {
      MBProgressHUD.hide(for: self.view, animated: true)
    }
  }
  
  func displayTodoList(viewModel: Main.GetTodoList.ViewModel) {
    displayMode = viewModel.style
    menuViewController?.displayMode = viewModel.style
    
    switch viewModel.content {
    case .success(let cells):
      mainTableView.backgroundView?.isHidden = true
      todoList = cells
    case .error:
      break
    case .empty:
      todoList = []
      mainTableView.backgroundView?.isHidden = false
    default: break
    }
    
    mainTableView.reloadData()
  }
  
  func displayDeleteTask(viewModel: Main.DeleteTodoTask.ViewModel) {
    switch viewModel.content {
    case .success:
      todoList.remove(at: viewModel.index)
      mainTableView.deleteRows(at: [IndexPath(row: viewModel.index, section: 0)], with: .automatic)
    default:
      break
    }
  }
  
  func displayTaskDetail(viewModel: Main.GetTaskDetail.ViewModel) {
    router?.navigateToTaskView(task: viewModel.content)
  }
  
  func displayAllTaskDone(viewModel: Main.SetAllTaskDone.ViewModel) {
    switch viewModel.content {
    case .success(let cells):
      selectedList.updateValue(false, forKey: viewModel.completedIndex)
      todoList = cells
      mainTableView.reloadData()
    case .loading:
      isUpdatingTask = true
    default:
      isUpdatingTask = false
      selectedList.removeAll()
      mainTableView.reloadData()
    }
  }
  
  func displayLogout(viewModel: Main.Logout.ViewModel) {
    router?.navigateToLogin()
  }
  
  @objc private func markAllTaskDone() {
    let filtered = selectedList.filter({ $0.value }).map({ $0.key })
    interactor?.setAllTaskDone(request: Main.SetAllTaskDone.Request(index: filtered))
  }
  
  @objc private func navigateToDoneTask() {
    router?.navigateToDoneTaskPage()
  }
  
  @objc private func navigateToLogin() {
    interactor?.requestLogout(request: Main.Logout.Request())
  }
}

// MARK: Setup
extension MainViewController {
  private func setup() {
    let viewController = self
    
    let presenter = MainPresenter()
    presenter.viewController = viewController
    
    let interactor = MainInteractor()
    interactor.worker = TodoListWorker(with: TodoListStore())
    interactor.taskWorker = TaskWorker(with: TodoTaskStore())
    interactor.presenter = presenter
    
    let router = MainRouter()
    router.viewController = viewController
    router.dataStore = interactor
    
    viewController.interactor = interactor
    viewController.router = router
  }
}

// MARK: Routing
extension MainViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let scene = segue.identifier {
      if scene == "menuContainer" {
        menuViewController = segue.destination as? MenuViewController
      }
    }
  }
}

// MARK: Main Table View Delegate & Data Source
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return todoList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell: ListCell = tableView.dequeueReusableCell(withIdentifier: ListCell.identifier) as? ListCell, todoList.indices.contains(indexPath.row) else {
      return UITableViewCell.init(style: .default, reuseIdentifier: nil)
    }
    let viewModel = todoList[indexPath.row]
    cell.updateUI(index: indexPath.row, viewModel: viewModel, isSelected: selectedList[indexPath.row] ?? false)
    cell.delegate = self
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return displayMode == .listMode
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == .delete) {
      let request = Main.DeleteTodoTask.Request(index: indexPath.row)
      interactor?.deleteTodoTask(request: request)
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let request = Main.GetTaskDetail.Request(index: indexPath.row, style: displayMode)
    interactor?.getTaskDetail(request: request)
  }
}

extension MainViewController: ListCellProtocol {
  func setSelectedTask(index: Int, isSelected: Bool) {
    selectedList.updateValue(isSelected, forKey: index)
  }
}

extension MainViewController: MenuViewProtocol {
  func didTapMenuButton(action: ButtonTag) {
    switch action {
    case .checkMark:
      interactor?.getTodoList(request: Main.GetTodoList.Request(style: .editMode(options: nil)))
    case .calendar:
      if case .listMode = displayMode {
        interactor?.getTodoList(request: Main.GetTodoList.Request(style: .calendarMode))
      } else if case .calendarMode = displayMode {
        interactor?.getTodoList(request: Main.GetTodoList.Request(style: .listMode))
      }
    case .addTodo:
      router?.navigateToTaskView(task: nil)
    case .markAllDone:
      for (index,_) in todoList.enumerated() {
        selectedList.updateValue(true, forKey: index)
      }
      mainTableView.reloadData()
    default:
      selectedList.removeAll() // Remove all selected list from previous scene
      interactor?.getTodoList(request: Main.GetTodoList.Request(style: .listMode))
    }
  }
}

extension MainViewController: TaskViewProtocal {
  func didUpdateTask(options: EditOptions) {
    interactor?.getTodoList(request: Main.GetTodoList.Request(style: .editMode(options: options)))
  }
}

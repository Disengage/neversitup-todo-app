//
//  MenuViewController.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 10/10/2564 BE.
//

import UIKit

enum ButtonTag: Int {
  case checkMark = 1
  case calendar = 2
  case addTodo = 3
  case back = 4
  case markAllDone = 5
}

protocol MenuViewProtocol {
  func didTapMenuButton(action: ButtonTag)
}

class MenuViewController: UIViewController {
  
  var delegate: MenuViewProtocol?
  
  var displayMode: DisplayMode = .listMode {
    didSet {
      switch displayMode {
      case .calendarMode:
        checkMarkButton.isHidden = true
        calendarButton.isHidden = false
        addTodoButton.isHidden = true
      default:
        checkMarkButton.isHidden = false
        calendarButton.isHidden = false
        addTodoButton.isHidden = false
      }
    }
  }
  
  private enum MenuState {
    case defaultView
    case actionView
  }
  
  private var menuState: MenuState = .defaultView {
    didSet {
      if case .defaultView = menuState {
        defaultMenuView.isHidden = false
        actionMenuView.isHidden = true
      } else {
        defaultMenuView.isHidden = true
        actionMenuView.isHidden = false
      }
    }
  }
  
  @IBOutlet private weak var defaultMenuView: UIView!
  @IBOutlet private weak var actionMenuView: UIView!
  
  @IBOutlet private weak var checkMarkButton: UIButton!
  @IBOutlet private weak var calendarButton: UIButton!
  @IBOutlet private weak var addTodoButton: UIButton!
  
  @IBOutlet private weak var backToDefaultButton: UIButton!
  @IBOutlet private weak var markAllDoneButton: UIButton!
  
  override func viewDidLoad() {
    menuState = .defaultView
    [checkMarkButton, calendarButton, addTodoButton, backToDefaultButton, markAllDoneButton].forEach { (button) in
      if let button = button {
        button.layer.cornerRadius = button.frame.width / 2
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowOffset = .zero
        button.layer.shadowRadius = 2
      }
    }
    [defaultMenuView, actionMenuView].forEach { (view) in
      view?.backgroundColor = UIColor.clear
    }
  }
  
  @IBAction func didTabMenuButton(_ sender: UIButton) {
    guard let senderButton = ButtonTag(rawValue: sender.tag) else { return }
    switch senderButton {
    case .checkMark:
      menuState = .actionView
    case .calendar:
      break
    case .addTodo:
      break
    case .back:
      menuState = .defaultView
    case .markAllDone:
      break
    }
    delegate?.didTapMenuButton(action: senderButton)
  }
}

//
//  ListCell.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import UIKit

struct DisplayedListCell {
  let description: String
  let date: String
  let time: String
  let style: DisplayMode
}

protocol ListCellProtocol {
  func setSelectedTask(index: Int, isSelected: Bool)
}

class ListCell: UITableViewCell {
  
  public static let identifier: String = "ListCell"
  
  var delegate: ListCellProtocol?
  
  @IBOutlet weak var wrapperView: UIView!
  
  @IBOutlet private weak var lblDesc: UILabel!
  @IBOutlet private weak var lblDate: UILabel!
  @IBOutlet private weak var lblTime: UILabel!
  
  @IBOutlet weak var dateTimeStackView: UIStackView!
  @IBOutlet weak var checkBoxView: UIView!
  @IBOutlet weak var checkBoxButton: UIButton!
  
  private var viewState: DisplayMode = .listMode {
    didSet {
      dateTimeStackView.isHidden = false
      checkBoxView.isHidden = true
      if case .editMode = viewState {
        dateTimeStackView.isHidden = true
      }
      if case .editMode = viewState {
        checkBoxView.isHidden = false
      }
    }
  }
  
  private var index: Int = 0
  
  override var isSelected: Bool {
    didSet {
      checkBoxButton.setTitle(isSelected ? "✔︎" : "" , for: .normal)
    }
  }
  
  override func awakeFromNib() {
    viewState = .listMode
    
    wrapperView.backgroundColor = UIColor.white
    wrapperView.layer.cornerRadius = 8
    wrapperView.layer.shadowColor = UIColor.lightGray.cgColor
    wrapperView.layer.shadowOpacity = 1
    wrapperView.layer.shadowOffset = .zero
    wrapperView.layer.shadowRadius = 1
    
    checkBoxButton.layer.cornerRadius = 4
    checkBoxButton.layer.shadowColor = UIColor.lightGray.cgColor
    checkBoxButton.layer.shadowOpacity = 1
    checkBoxButton.layer.shadowOffset = .zero
    checkBoxButton.layer.shadowRadius = 2
  }
  
  func updateUI(index: Int, viewModel: DisplayedListCell, isSelected: Bool) {
    viewState = viewModel.style
    lblDesc.text = viewModel.description
    lblDate.text = viewModel.date
    lblTime.text = viewModel.time
    self.index = index
    self.isSelected = isSelected
  }
  
  @IBAction func didTapCheckBox(_ sender: Any) {
    isSelected = !isSelected
    delegate?.setSelectedTask(index: index, isSelected: isSelected)
  }
}

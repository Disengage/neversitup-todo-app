//
//  TodoListStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation
import Alamofire

class TodoListStore: TodoListProtocol {
  enum EndPoint: String {
    case getTodoList = "task"
  }
  
  func getTodoList(completion: @escaping (Result<TodoList>) -> Void) {
    let path = EndPoint.getTodoList.rawValue.withBaseURL()
    AF.request(path, method: .get, parameters: nil, headers: HttpHelper.commonHeader()).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: TodoList.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
      }
  }
}

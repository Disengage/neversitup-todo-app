//
//  TodoListStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation
import Alamofire

class TodoTaskStore: TodoTaskProtocal {
  enum EndPoint: String {
    case task = "task"
  }
  
  func addNewTask(desc: String, completion: @escaping (Result<GetTask>) -> Void) {
    let path = EndPoint.task.rawValue.withBaseURL()
    let params = ["description": desc]
    AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default, headers: HttpHelper.commonHeader()).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: GetTask.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
      }
  }
  
  func updateTask(id: String, request: UpdateRequest, completion: @escaping (Result<GetTask>) -> Void) {
    let path = EndPoint.task.rawValue.appending("/\(id)") .withBaseURL()
    AF.request(path, method: .put, parameters: request, encoder: JSONParameterEncoder.default, headers: HttpHelper.commonHeader()).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: GetTask.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
      }
  }
  
  func deleteTask(id: String, completion: @escaping (Result<UpdateTask>) -> Void) {
    let path = EndPoint.task.rawValue.appending("/\(id)").withBaseURL()
    AF.request(path, method: .delete, parameters: nil, headers: HttpHelper.commonHeader()).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: UpdateTask.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
      }
  }
}

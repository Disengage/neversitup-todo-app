//
//  UserStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation
import Alamofire

class UserStore: UserProtocol {
  enum EndPoint: String {
    case login = "user/login"
    case me = "user/me"
    case register = "user/register"
  }
  
  func register(request: Register, completion: @escaping (Result<User>) -> Void) {
    let path = EndPoint.register.rawValue.withBaseURL()
    AF.request(path, method: .post, parameters: request, encoder: JSONParameterEncoder.default, headers: nil).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: User.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
    }
  }
  
  func login(request: Login, completion: @escaping (Result<User>) -> Void) {
    let path = EndPoint.login.rawValue.withBaseURL()
    AF.request(path, method: .post, parameters: request, encoder: JSONParameterEncoder.default, headers: nil).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: User.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
    }
  }
  
  func validateToken(token: String, completion: @escaping (Result<UserInfo>) -> Void) {
    let path = EndPoint.me.rawValue.withBaseURL()
    AF.request(path, method: .get, parameters: nil, headers: HttpHelper.commonHeader()).validate()
      .responseString(completionHandler: { (response) in
        print(response.result)
      })
      .responseDecodable(of: UserInfo.self) { (response) in
        switch response.result {
        case .success(let data):
          completion(.success(result: data))
        case .failure(let error):
          print(error)
          completion(.failure(error: error))
        }
    }
  }
}

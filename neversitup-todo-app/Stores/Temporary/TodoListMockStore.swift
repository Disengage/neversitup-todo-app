//
//  TodoListMockStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

class TodoListMockStore: TodoListProtocol {
  func getTodoList(completion: @escaping (Result<TodoList>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "task", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let todoList = try JSONDecoder().decode(TodoList.self, from: data)
        completion(.success(result: todoList))
      } catch (let error) {
        completion(.failure(error: error))
      }
    }
  }
}

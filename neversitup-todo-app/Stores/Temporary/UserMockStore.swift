//
//  UserMockStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

class UserMockStore: UserProtocol {
  func register(request: Register, completion: @escaping (Result<User>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "register", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let login = try JSONDecoder().decode(User.self, from: data)
        completion(.success(result: login))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
  
  func login(request: Login, completion: (Result<User>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "login", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let login = try JSONDecoder().decode(User.self, from: data)
        completion(.success(result: login))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
  
  func validateToken(token: String, completion: @escaping (Result<UserInfo>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "user_me", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let login = try JSONDecoder().decode(UserInfo.self, from: data)
        completion(.success(result: login))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
}

//
//  TodoListMockStore.swift
//  neversitup-todo-app
//
//  Created by Narongsak Kongpan on 9/10/2564 BE.
//

import Foundation

class TodoTaskMockStore: TodoTaskProtocal {
  func addNewTask(desc: String, completion: @escaping (Result<GetTask>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "add_task", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let task = try JSONDecoder().decode(GetTask.self, from: data)
        completion(.success(result: task))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
  
  func updateTask(id: String, request: UpdateRequest, completion: @escaping (Result<GetTask>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "update_task", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let task = try JSONDecoder().decode(GetTask.self, from: data)
        completion(.success(result: task))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
  
  func deleteTask(id: String, completion: @escaping (Result<UpdateTask>) -> Void) {
    if let path: String = Bundle.main.path(forResource: "delete_task", ofType: "json"), let data = dataFrom(path: path) {
      do {
        let todoList = try JSONDecoder().decode(UpdateTask.self, from: data)
        completion(.success(result: todoList))
      } catch (let error) {
        completion(.failure(error: error))
      }
    } else {
      completion(.failure(error: nil))
    }
  }
}
